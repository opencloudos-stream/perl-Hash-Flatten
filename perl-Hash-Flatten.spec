Name:           perl-Hash-Flatten
Version:        1.19
Release:    3%{?dist}
Summary:        Flatten/unflatten complex data hashes
License:        GPL-2.0-only
URL:            https://metacpan.org/release/Hash-Flatten
Source0:        https://cpan.metacpan.org/authors/id/B/BB/BBC/Hash-Flatten-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils
BuildRequires:  glibc-common
BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Carp)
BuildRequires:  perl(constant)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(overload)
BuildRequires:  perl(strict)
BuildRequires:  perl(vars)
BuildRequires:  perl(Getopt::Std)
BuildRequires:  perl(Log::Trace)
BuildRequires:  perl(Test::Assertions)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Test::Pod) >= 1.00
BuildRequires:  perl(Test::Pod::Coverage) >= 1.00
Recommends:     perl(overload)

%description
Converts back and forth between a nested hash structure and a flat hash of
delimited key-value pairs. Useful for protocols that only support key-value
pairs (such as CGI and DBMs).

%prep
%setup -q -n Hash-Flatten-%{version}
iconv -f latin1 -t utf8 Changes > Changes.utf && \
    touch Changes.utf -r Changes && \
    mv Changes.utf Changes

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license COPYING
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Tue Jul 02 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.19-3
- rebuild

* Mon Jul 01 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.19-2
- rebuild

* Thu Jun 20 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.19-1
- initial build
